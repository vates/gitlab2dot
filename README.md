Parse all issues in a repo and export into a dot file.

## Usage

`./gitlab2dot --token <token> --output <fileName> --project <projetId> [--ids]`

With:

```

<token> = gitlab private token or personnal access token

<fileName> = a *.dot file to write the output

<project> = a gitlab project id (integer)

--ids with id numbers displayed
```