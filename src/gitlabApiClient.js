import assign from 'lodash.assign'
import Bluebird from 'bluebird'
import request from 'request'
import size from 'lodash.size'
import first from 'lodash.first'
import fs from 'fs-extra'

Bluebird.promisifyAll(request)

export default class GitLabClient {
  constructor (url, token) {
    this._url = url
    this._token = token
  }

  _get (...args) {
    const qs = {'per_page': 100}
    const _qs = first(args)
    if (typeof _qs === 'object') {
      assign(qs, _qs)
      args.shift()
    }
    const path = args.join('/')
    return request.getAsync({
      url: `${this._url}/${path}`,
      json: true,
      headers: {
        'PRIVATE-TOKEN': this._token
      },
      qs
    })
    .then(({response, body}) => {
      if (body.error) {
        throw new Error(body.error)
      }
      return body
    })
  }

  _post (params, ...args) {
    const path = args.join('/')
    const data = {
      url: `${this._url}/${path}`,
      json: true,
      headers: {
        'PRIVATE-TOKEN': this._token
      }
    }
    if (size(params)) {
      data.qs = params
    }
    return request.postAsync(data)
    .then(([response, body]) => {
      if (body.error) {
        throw new Error(body.error)
      }
      return body
    })
  }

  _put (params, ...args) {
    const path = args.join('/')
    const data = {
      url: `${this._url}/${path}`,
      json: true,
      headers: {
        'PRIVATE-TOKEN': this._token
      }
    }
    if (size(params)) {
      data.qs = params
    }
    return request.putAsync(data)
    .then(([response, body]) => {
      if (body.error) {
        throw new Error(body.error)
      }
      return body
    })
  }

  _form (params, ...args) {
    const path = args.join('/')
    return request.postAsync({
      url: `${this._url}/${path}`,
      form: params,
      json: true,
      headers: {
        'PRIVATE-TOKEN': this._token
      }
    })
    .then(([response, body]) => {
      if (body.error) {
        throw new Error(body.error)
      }
      return body
    })
  }

  _multi (file, ...args) {
    const path = args.join('/')
    return request.postAsync({
      url: `${this._url}/${path}`,
      formData: {
        file: {
          value: fs.createReadStream(file.path),
          options: {
            filename: file.name,
            contentType: file.type
          }
        }
      },
      json: true,
      headers: {
        'PRIVATE-TOKEN': this._token
      }
    })
    .then(([response, body]) => {
      if (body.error) {
        throw new Error(body.error)
      }
      return body
    })
  }

  getProjects () {
    return this._get('projects')
  }

  getProject (id) {
    return this._get('projects', id)
  }

  getProjectLabels (id) {
    return this._get('projects', id, 'labels')
  }

  createProjectLabel (projectId, name, color = '#D3D3D3') {
    return this._form({name, color}, 'projects', projectId, 'labels')
  }

  getProjectIssues (id, filter = {}) {
    return this._get(filter, 'projects', id, 'issues')
  }

  getProjectIssue (projectId, issueId) {
    return this._get('projects', projectId, 'issues', issueId)
  }

  getProjectIssueLinks (projectId, issueId) {
    return this._get('projects', projectId, 'issues', issueId, 'links')
  }

  createProjectIssue (projectId, title, description, labels = []) {
    const params = {title, description}
    if (size(labels)) {
      params.labels = labels.join(',')
    }
    return this._post(params, 'projects', projectId, 'issues')
  }

  updateProjectIssue (projectId, issueId, params) {
    return this._put(params, 'projects', projectId, 'issues', issueId)
  }

  getProjectIssueNotes (projectId, issueId) {
    return this._get('projects', projectId, 'issues', issueId, 'notes')
  }

  createProjectIssueNote (projectId, issueId, body) {
    return this._post({body}, 'projects', projectId, 'issues', issueId, 'notes')
  }

  uploadProjetFile (projectId, file) {
    return this._multi(file, 'projects', projectId, 'uploads')
  }
}
