#!/usr/bin/env node
import Bluebird from 'bluebird'
import GitlabApiClient from './gitlabApiClient'
import first from 'lodash.first'
import forEach from 'lodash.foreach'
import includes from 'lodash.includes'
import fs from 'fs-extra'

// globals
const raw = {}
const issues = {}
const links = []
const labels = {_default: '#FFFFFF'}

const reqRegExp = /Requires(( #([0-9]+))+)/

const storeIssue = function (issue) {
  const _issue = issues[_id(issue)]
  if (_issue) {
    return _issue
  }
  const label = first(issue.labels)
  const fillColor = labels[label || '_default']
  let color
  color = fillColor
  label || (color = '#000000')
  issue.state === 'closed' && (color = '#FF0000')
  let title = issue.title
  idsOpt && (title += ` #${issue.iid}`)
  const newIssue = {id: +issue.iid, title, fillColor, color}
  issues[newIssue.id] = newIssue
  return newIssue
}

const areLinked = function (i1, i2) {
  const l1to2 = formatLink(i1, i2)
  const l2to1 = formatLink(i2, i1)
  return (includes(links, l1to2) || includes(links, l2to1))
}

const formatLink = function (i1, i2) {
  return `${_id(i1)}>${_id(i2)}`
}

const storeLink = function (p, c) {
  const parent = storeIssue(p)
  const child = storeIssue(c)
  if (areLinked(parent, child)) {
    return
  }
  const pToC = formatLink(parent, child)
  links.push(pToC)
}

const processIssue = function (issue, parents) {
  return processLinks(issue)
}

const findRequirements = function (description) {
  const matches = description && description.match(reqRegExp)
  const reqs = matches && matches[1] && matches[1].split(' #')
  return reqs
}

const processLinks = function (issue) {
  const {description} = issue
  const reqs = findRequirements(description)
  forEach(reqs, r => {
    if (r) {
      const req = raw[r]
      if (req) {
        storeLink(req, issue)
      }
    }
  })
  return
}

const _id = function (issue) {
  return +issue.iid || +issue.id
}

const argv = process.argv.slice(2)
let index
let token
let output
let project

const tokenOpt = (index = argv.indexOf('--token')) !== -1
tokenOpt && argv.splice(index, 1) && (token = argv[index]) && argv.splice(index, 1)
const outputOpt = (index = argv.indexOf('--output')) !== -1
outputOpt && argv.splice(index, 1) && (output = argv[index]) && argv.splice(index, 1)
const projectOpt = (index = argv.indexOf('--project')) !== -1
projectOpt && argv.splice(index, 1) && (project = argv[index]) && argv.splice(index, 1)
const idsOpt = (index = argv.indexOf('--ids')) !== -1
idsOpt && argv.splice(index, 1)

const gitlabApiClient = new GitlabApiClient('https://gitlab.com/api/v4', token)

gitlabApiClient.getProjectLabels(project)
.then(lbls => {
  forEach(lbls, l => {
    labels[l.name] = l.color
  })
  return
})
.then(() => {
  gitlabApiClient.getProjectIssues(project)
  .then(data => {
    forEach(data, d => raw[d.iid] = d)
    const promises = []
    forEach(raw, issue => promises.push(processIssue(issue, argv)))
    Bluebird.all(promises).then(() => {
      let dot = ''
      forEach(issues, f => {
        dot += `${f.id} [label = "${f.title}",style="filled",fillcolor="${f.fillColor}",color="${f.color}"]
`
      })
      forEach(links, l => {
        const [p, c] = l.split('>')
        dot += `${p} -> ${c};
`
      })
      dot = `digraph G {
${dot}}`
      fs.writeFile(output, dot)
    })
  })
})
